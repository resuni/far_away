# far_away
An i3 style with colors based on "Somewhere far away" by nShii on DeviantArt: http://nshii.deviantart.com/art/Somewhere-far-away-617065169

Terminal colors are configured for Rxvt-unicode. This configuration is found in the .Xresources file.
